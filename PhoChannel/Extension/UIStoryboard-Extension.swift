//
//  UIStoryboard-Extension.swift
//  PhoChannel
//
//  Created by macOS on 6/14/18.
//  Copyright © 2018 macOS. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    static var MAIN: String { return "Main" }
    
    class func storyboardWithName(name: String) -> UIStoryboard {
        return UIStoryboard(name: name, bundle: Bundle.main)
    }
    
    /*
     * MAIN STORYBOARD
     */
    static var videoListViewController: VideoListViewController {
        return self.storyboardWithName(name: MAIN)
            .instantiateViewController(withIdentifier: "VideoListViewController") as! VideoListViewController
    }
    
    static var videoDetailViewController: VideoDetailViewController {
        return self.storyboardWithName(name: MAIN)
            .instantiateViewController(withIdentifier: "VideoDetailViewController") as! VideoDetailViewController
    }
    
}
