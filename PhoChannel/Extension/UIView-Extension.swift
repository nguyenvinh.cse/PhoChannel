//
//  UIView-Extension.swift
//  PhoChannel
//
//  Created by macOS on 6/14/18.
//  Copyright © 2018 macOS. All rights reserved.
//

import UIKit

extension UIView {
    
    var myViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let parentView = parentResponder as? UIViewController {
                return parentView
            }
        }
        return nil
    }
    
    //MARK: layer
    
    func createCornerRadius(_ radius: CGFloat) {
        self.layer.cornerRadius = radius
    }
    
    func createBorderColor(_ color: UIColor) {
        self.layer.borderColor = color.cgColor
    }
    
    func createBorderWidth(_ width: CGFloat) {
        self.layer.borderWidth = width;
    }
    
    func createCricleRadius() {
        self.layer.cornerRadius = self.frame.width / 2
        self.clipsToBounds = true
    }
    
    func createBorderWithColor(border: CGFloat, color: UIColor) {
        self.createBorderColor(color)
        self.createBorderWidth(border)
    }
    
}
