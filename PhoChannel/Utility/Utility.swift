//
//  Utility.swift
//  PhoChannel
//
//  Created by macOS on 6/14/18.
//  Copyright © 2018 macOS. All rights reserved.
//

import Foundation
import MBProgressHUD

let Utils = Utility.share

class Utility: NSObject {
    static let share = Utility()
}

// MARK: MBProgressHUD
extension Utility {
    
    func showProgressHUD(inView view: UIView, text: String = "") {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            let progressHUD = MBProgressHUD.showAdded(to: view, animated: true)
            progressHUD.mode = .indeterminate
            progressHUD.label.text = text
        }
    }
    
    func hideProgressHUD(inView view: UIView) {
        DispatchQueue.global(qos: .userInitiated).async {
            // Bounce back to the main thread to update the UI
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                MBProgressHUD.hide(for: view, animated: true)
            }
        }
    }
    
}

//MARK: GET API
extension Utility {
    
    func getRequest(urlString: String, completion: @escaping (_ dictionary: [String: Any]) -> Void) {
        guard let requestUrl = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: requestUrl, completionHandler: { (data, response, error) in
            guard error == nil else {
                print(error!)
                return
            }
            
            if let usableData = data {
                print(usableData) //JSONSerialization
                do {
                    let json = try JSONSerialization.jsonObject(with: usableData, options: .mutableContainers)
                    if let dictionary = json as? [String: Any] {
                        completion(dictionary)
                    }
                }
                catch let jsonError {
                    print(jsonError)
                }
            }
        }).resume()
    }
    
}
