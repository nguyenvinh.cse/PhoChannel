//
//  VideoListViewCell.swift
//  PhoChannel
//
//  Created by macOS on 6/14/18.
//  Copyright © 2018 macOS. All rights reserved.
//

import UIKit

class VideoListViewCell: UICollectionViewCell {
    
    @IBOutlet weak var videoThumbnail: VideoThumbnailView!
    @IBOutlet weak var channelAvatar: UIImageView!
    @IBOutlet weak var videoTitle: UILabel!
    @IBOutlet weak var videoDescription: UILabel!
    
    var videoInfo: VideoModel? {
        didSet {
            if let info = self.videoInfo {
                self.videoTitle.text = info.snippet?.title
                self.videoDescription.text = info.snippet?.description
                self.setupThumbnail()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        channelAvatar.createCricleRadius()
    }
    
    func setupThumbnail() {
        if let thumbnail = self.videoInfo?.snippet?.thumbnails?.high?.url {
            self.videoThumbnail.loadImageUsingUrlString(urlString: thumbnail)
        }
    }
    
}
