//
//  VideoRelatedViewCell.swift
//  PhoChannel
//
//  Created by macOS on 6/15/18.
//  Copyright © 2018 macOS. All rights reserved.
//

import UIKit

class VideoRelatedViewCell: UITableViewCell {
    
    @IBOutlet weak var videoThumbnail: VideoThumbnailView!
    @IBOutlet weak var videoTitle: UILabel!
    @IBOutlet weak var videoDescription: UILabel!
    @IBOutlet weak var videoPlayButton: UIButton!
    
    var videoInfo: VideoModel? {
        didSet {
            if let video = videoInfo {
                self.setupThumbnail()
                self.videoTitle.text = video.snippet?.title ?? ""
                self.videoDescription.text = video.snippet?.description ?? ""
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        videoPlayButton.addTarget(self, action: #selector(self.handlePlayVideo(_:)), for: .touchUpInside)
    }
    
    @objc func handlePlayVideo(_ sender: UIButton) {
        if let vc = self.myViewController as? VideoDetailViewController {
            vc.didSelectVideoRelated(self.videoInfo!)
        }
    }
    
    func setupThumbnail() {
        if let thumbnail = self.videoInfo?.snippet?.thumbnails?.defaults?.url {
            self.videoThumbnail.loadImageUsingUrlString(urlString: thumbnail)
        }
    }
    
}
