//
//  VideoInfoViewCell.swift
//  PhoChannel
//
//  Created by macOS on 6/15/18.
//  Copyright © 2018 macOS. All rights reserved.
//

import UIKit

class VideoInfoViewCell: UITableViewCell {
    
    @IBOutlet weak var videoTitle: UILabel!
    @IBOutlet weak var iconCollapse: UIImageView!
    @IBOutlet weak var collapseButton: UIButton!
    @IBOutlet weak var channelTitle: UILabel!
    @IBOutlet weak var videoDescription: UILabel!
    
    var isCollapse: Bool = false
    
    var videoInfo: VideoModel? {
        didSet {
            if let video = videoInfo {
                self.videoTitle.text = video.snippet?.title ?? ""
                self.channelTitle.text = video.snippet?.channelTitle ?? ""
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        hiddenDescription()
        collapseButton.addTarget(self, action: #selector(self.handleCollapseButton(_:)), for: .touchUpInside)
    }
    
    func hiddenDescription() {
        // Update icon
        iconCollapse.image = UIImage(named: "icon_down_arrow")
        // Handle description
        videoDescription.text = ""
        videoDescription.numberOfLines = 1
        videoDescription.lineBreakMode = .byTruncatingTail
    }
    
    func showDescription(_ text: String) {
        // Update icon
        iconCollapse.image = UIImage(named: "icon_up_arrow")
        // Handle description
        videoDescription.text = text
        videoDescription.numberOfLines = 0
        videoDescription.lineBreakMode = .byWordWrapping
    }
    
    @objc func handleCollapseButton(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5, animations: {
            self.isCollapse = !self.isCollapse
            if self.isCollapse {
                self.showDescription(self.videoInfo?.snippet?.description ?? "")
            }
            else {
                self.hiddenDescription()
            }
            
            if let vc = self.myViewController as? VideoDetailViewController {
                vc.tableView.reloadData()
            }
        })
    }
    
}
