//
//  ThumbnailSize.swift
//  PhoChannel
//
//  Created by macOS on 6/14/18.
//  Copyright © 2018 macOS. All rights reserved.
//

import Foundation

class ThumbnailSize {    
    var defaults: ThumbnailModel?
    var medium: ThumbnailModel?
    var high: ThumbnailModel?
    
    init(dictionary: [String: Any]) {
        if let defaultDic = dictionary["default"] as? [String: Any] {
            defaults = ThumbnailModel(dictionary: defaultDic)
        }
        if let mediumDic = dictionary["medium"] as? [String: Any] {
            medium = ThumbnailModel(dictionary: mediumDic)
        }
        if let highDic = dictionary["high"] as? [String: Any] {
            high = ThumbnailModel(dictionary: highDic)
        }
    }
}
