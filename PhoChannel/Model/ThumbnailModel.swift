//
//  ThumbnailModel.swift
//  PhoChannel
//
//  Created by macOS on 6/14/18.
//  Copyright © 2018 macOS. All rights reserved.
//

import Foundation

class ThumbnailModel {
    var url : String = ""
    var width : Int = 0
    var height : Int = 0
    
    init(dictionary: [String: Any]) {
        url = dictionary["url"] as? String ?? ""
        width = dictionary["width"] as? Int ?? 0
        height = dictionary["height"] as? Int ?? 0
    }
}
