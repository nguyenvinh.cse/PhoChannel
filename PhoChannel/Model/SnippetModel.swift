//
//  SnippetModel.swift
//  PhoChannel
//
//  Created by macOS on 6/14/18.
//  Copyright © 2018 macOS. All rights reserved.
//

import Foundation

class SnippetModel {
    var publishedAt: String = ""
    var channelId: String = ""
    var title: String = ""
    var description: String = ""
    var channelTitle: String = ""
    var thumbnails: ThumbnailSize?
    
    init(dictionary: [String: Any]) {
        publishedAt = dictionary["publishedAt"] as? String ?? ""
        channelId = dictionary["channelId"] as? String ?? ""
        title = dictionary["title"] as? String ?? ""
        description = dictionary["description"] as? String ?? ""
        channelTitle = dictionary["channelTitle"] as? String ?? ""
        
        if let thumbnailsDic = dictionary["thumbnails"] as? [String: Any] {
            thumbnails = ThumbnailSize(dictionary: thumbnailsDic)
        }
    }
}
