//
//  VideoIdentifier.swift
//  PhoChannel
//
//  Created by macOS on 6/14/18.
//  Copyright © 2018 macOS. All rights reserved.
//

import Foundation

class VideoIdentifier {
    var kind : String = ""
    var videoId : String = ""
    
    init(dictionary: [String: Any]) {
        kind = dictionary["kind"] as? String ?? ""
        videoId = dictionary["videoId"] as? String ?? ""
    }
}
