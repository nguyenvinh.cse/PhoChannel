//
//  VideoModel.swift
//  PhoChannel
//
//  Created by macOS on 6/14/18.
//  Copyright © 2018 macOS. All rights reserved.
//

import Foundation

class VideoModel {
    var id: VideoIdentifier?
    var snippet: SnippetModel?
    
    init(dictionary: [String: Any]) {
        if let idDic = dictionary["id"] as? [String: Any] {
            id = VideoIdentifier(dictionary: idDic)
        }
        if let snippetDic = dictionary["snippet"] as? [String: Any] {
            snippet = SnippetModel(dictionary: snippetDic)
        }
    }
}
