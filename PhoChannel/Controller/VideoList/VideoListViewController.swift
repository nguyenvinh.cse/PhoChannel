//
//  VideoListViewController.swift
//  PhoChannel
//
//  Created by macOS on 6/14/18.
//  Copyright © 2018 macOS. All rights reserved.
//

import UIKit

enum VideoListSection: Int {
    case Video = 0, More
    static let allValues = [Video, More]
}

class VideoListViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var noDataLabel: UILabel!
    
    var listVideo: [VideoModel] = [VideoModel]()
    var nextPageToken: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Phở's Channel"
        // Do any additional setup after loading the view.
        setupView()
        loadData()
    }
    
    func setupView() {
        self.noDataLabel.isHidden = false
        self.registerCollectionCell()
    }
    
    func registerCollectionCell() {
        collectionView.register(UINib.init(nibName: "VideoListViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        collectionView.register(UINib.init(nibName: "LoadMoreCollectionCell", bundle: nil), forCellWithReuseIdentifier: "cell_more")
    }
    
    func loadData() {
        Utils.showProgressHUD(inView: self.collectionView, text: "Loading...")
        fetchVideo("")
    }
    
}

//MARK: Collection view
extension VideoListViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return VideoListSection.allValues.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case VideoListSection.More.rawValue:
            return (self.listVideo.count > 0 && self.nextPageToken != nil) ? 1 : 0
            
        default:
            return listVideo.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section {
        case VideoListSection.More.rawValue:
            return CGSize(width: self.view.frame.width, height: 60)
            
        default:
            let height = ((self.view.frame.width - 10 - 10) * 9 / 16) + CGFloat(90)
            return CGSize(width: self.view.frame.width, height: height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case VideoListSection.More.rawValue:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell_more", for: indexPath) as! LoadMoreCollectionCell
            cell.loading.startAnimating()
            return cell
            
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! VideoListViewCell
            let video = listVideo[indexPath.item]
            cell.videoInfo = video
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == VideoListSection.Video.rawValue {
            let videoDetailCV = UIStoryboard.videoDetailViewController
            videoDetailCV.videoPlay = self.listVideo[indexPath.item]
            videoDetailCV.isPlayVideo = true
            self.present(videoDetailCV, animated: true, completion: nil)
        }
    }
    
}

//MARK: Get api
extension VideoListViewController {
    
    func fetchVideo(_ page: String) {
        let urlRequest = String(format: api_channel_video, API_KEY, CHANNEL_KEY, page)
        
        Utils.getRequest(urlString: urlRequest) { (dictionary) in
            Utils.hideProgressHUD(inView: self.collectionView)
            
            self.nextPageToken = nil
            if let nextPage = dictionary["nextPageToken"] as? String {
                self.nextPageToken = nextPage
            }
            
            if let videos = dictionary["items"] as? [[String: Any]] {
                videos.forEach({ (videoDic: [String: Any]) in
                    let newsData = VideoModel(dictionary: videoDic)
                    self.listVideo.append(newsData)
                })
                
                DispatchQueue.main.async {
                    if self.listVideo.count > 0 {
                        self.noDataLabel.isHidden = true
                    }
                    self.collectionView.reloadData()
                }
            }
        }
    }
    
    // Load more
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let nextPageToken = self.nextPageToken {
            self.fetchVideo(nextPageToken)
        }
    }
    
}
