//
//  VideoDetailViewController.swift
//  PhoChannel
//
//  Created by macOS on 6/15/18.
//  Copyright © 2018 macOS. All rights reserved.
//

import UIKit
import XCDYouTubeKit

enum VideoDetailSection: Int {
    case Info = 0, RelatedHeader, Related, More
    static let allValues = [Info, RelatedHeader, Related, More]
}

class VideoDetailViewController: UIViewController {
    
    @IBOutlet weak var videoThumbnail: VideoThumbnailView!
    @IBOutlet weak var playVideoButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dismissButton: UIButton!
    
    var videoPlay: VideoModel!
    var isPlayVideo: Bool = false
    
    var listVideoRelated: [VideoModel] = [VideoModel]()
    var nextPageToken: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup layout
        setupLayoutVideoPlay()
        setupButtonAction()
        setupTableView()
        
        // Load data
        loadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.isPlayVideo {
            self.isPlayVideo = false
            self.playVideo()
        }
    }
    
    func loadData() {
        Utils.showProgressHUD(inView: self.tableView, text: "Loading...")
        fetchVideo("")
    }
    
}

//MARK: Setup UI
extension VideoDetailViewController {
    
    func setupLayoutVideoPlay() {
        UIView.animate(withDuration: 1, animations: {
            if let thumbnail = self.videoPlay.snippet?.thumbnails?.high?.url {
                self.videoThumbnail.loadImageUsingUrlString(urlString: thumbnail)
            }
        })
    }
    
    func setupTableView() {
        initTableView()
        registerTableViewCell()
    }
    
    func setupButtonAction() {
        playVideoButton.addTarget(self, action: #selector(self.handlePlayVideoButton(_:)), for: .touchUpInside)
        dismissButton.addTarget(self, action: #selector(self.handleDismissButton(_:)), for: .touchUpInside)
    }
    
    @objc func handlePlayVideoButton(_ sender: UIButton) {
        playVideo()
    }
    
    @objc func handleDismissButton(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func playVideo() {
        let videoPlayer: XCDYouTubeVideoPlayerViewController = XCDYouTubeVideoPlayerViewController(videoIdentifier: self.videoPlay.id?.videoId ?? "")
        self.present(videoPlayer, animated: true, completion: nil)
    }
    
    func initTableView() {
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
    }
    
    func registerTableViewCell() {
        tableView.register(UINib.init(nibName: "VideoInfoViewCell", bundle: nil), forCellReuseIdentifier: "cell_info")
        tableView.register(UINib.init(nibName: "VideoRelatedTextCell", bundle: nil), forCellReuseIdentifier: "cell_relatedHeader")
        tableView.register(UINib.init(nibName: "VideoRelatedTextCell", bundle: nil), forCellReuseIdentifier: "cell_relatedNoData")
        tableView.register(UINib.init(nibName: "VideoRelatedViewCell", bundle: nil), forCellReuseIdentifier: "cell_related")
        tableView.register(UINib.init(nibName: "LoadMoreTableCell", bundle: nil), forCellReuseIdentifier: "cell_more")
    }
    
}

//MARK: Table view
extension VideoDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return VideoDetailSection.allValues.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case VideoDetailSection.Info.rawValue, VideoDetailSection.RelatedHeader.rawValue:
            return 1
        case VideoDetailSection.More.rawValue:
            return (self.listVideoRelated.count > 0 && self.nextPageToken != nil) ? 1 : 0
            
        default:
            return self.listVideoRelated.count > 0 ? self.listVideoRelated.count : 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case VideoDetailSection.RelatedHeader.rawValue, VideoDetailSection.More.rawValue:
            return 50
        case VideoDetailSection.Related.rawValue:
            return self.listVideoRelated.count > 0 ? 85 : 60
            
        default:
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case VideoDetailSection.Info.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell_info", for: indexPath) as! VideoInfoViewCell
            cell.selectionStyle = .none
            cell.videoInfo = self.videoPlay
            return cell
            
        case VideoDetailSection.RelatedHeader.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell_relatedHeader", for: indexPath) as! VideoRelatedTextCell
            cell.selectionStyle = .none
            
            cell.titleLabel.text = "VIDEO CÙNG LOẠI"
            cell.titleLabel.textAlignment = .left
            cell.titleLabel.font = UIFont(name: "HelveticaNeue", size: 18.0)
            
            return cell
            
        case VideoDetailSection.More.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell_more", for: indexPath) as! LoadMoreTableCell
            cell.selectionStyle = .none
            cell.loading.startAnimating()
            return cell
            
        default:
            if self.listVideoRelated.count > 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell_related", for: indexPath) as! VideoRelatedViewCell
                cell.selectionStyle = .none
                
                let videoRelated = self.listVideoRelated[indexPath.row]
                cell.videoInfo = videoRelated
                
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell_relatedNoData", for: indexPath) as! VideoRelatedTextCell
                cell.selectionStyle = .none
                
                cell.titleLabel.text = "Chưa có dữ liệu"
                cell.titleLabel.textAlignment = .center
                cell.titleLabel.font = UIFont(name: "HelveticaNeue", size: 15.0)
                
                return cell
            }
        }
    }
    
}

//MARK: Related video
extension VideoDetailViewController {
    
    func didSelectVideoRelated(_ video: VideoModel) {
        // Update video play
        self.videoPlay = video
        self.setupLayoutVideoPlay()
        // Reset videos related
        self.listVideoRelated.removeAll()
        self.tableView.reloadData()
        // Load videos related
        self.loadData()
        self.playVideo()
    }
    
}


//MARK: Get api
extension VideoDetailViewController {
    
    func fetchVideo(_ page: String) {
        let urlRequest = String(format: api_related_video, API_KEY, self.videoPlay.id?.videoId ?? 0, page)
        
        Utils.getRequest(urlString: urlRequest) { (dictionary) in
            Utils.hideProgressHUD(inView: self.tableView)
            
            self.nextPageToken = nil
            if let nextPage = dictionary["nextPageToken"] as? String {
                self.nextPageToken = nextPage
            }
            
            if let videos = dictionary["items"] as? [[String: Any]] {
                videos.forEach({ (videoDic: [String: Any]) in
                    let newsData = VideoModel(dictionary: videoDic)
                    self.listVideoRelated.append(newsData)
                })
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    // Load More    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let nextPageToken = self.nextPageToken {
            self.fetchVideo(nextPageToken)
        }
    }
    
}
